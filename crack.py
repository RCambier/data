#!python3
# "bad block cipher"

import sys
import random
import csv
from multiprocessing import Pool, Queue, Event
from time import sleep

sboxSize = 5
blockSize = 5
data = []
encrypt = {}
box = [22, 0, 19, 9, 15, 3, 21, 18, 4, 26, 28, 13, 27, 5, 25, 31, 29, 12, 24, 6, 23, 8, 2, 11, 16, 30, 14, 10, 20, 7, 17, 1]
invbox = [ 1,  31,  22,  5,  8,  13,  19,  29,  21,  3, 27,  23, 17, 11,  26, 4, 24,  30, 7, 2,  28,  6,  0,  20,  18,  14,  9,  12,  10, 16,  25,  15]
queue = Queue()
maxEncrypt = 16777215
#maxEncrypt = 177870
event = Event()

def myhex(state):
	return "0x%0.10X" % state

def addKey(state, key):
	expansion = 0
	for i in range(blockSize):  
		expansion  = expansion<<8
		key0 = key
		if (i%2==1):
			key0 = key ^ 0xFF
		expansion = expansion | key0
	return state ^ expansion

def sbox(state):
	result = 0
	loops = blockSize*8//sboxSize
	for i in range(loops):
		result=result<<sboxSize
		result = result | box[(state>>sboxSize*(loops-i-1))&0x1F]	
	return result

def encrypt_block(message, key, keySize):
	state = message
	for i in range(1,keySize):
		state = addKey(state, (key>>(8*(keySize-i)))&0xFF ) 
		state = sbox(state) 								
		tmp = (state& 0xFF) << 8*(blockSize-1) 				
		state = (state>>8) | tmp               				
	state = addKey(state, key&0xFF ) 
	return state

def encrypt_block_loop(message, key, keySize):
	state = message
	for i in range(1,keySize+1):
		state = addKey(state, (key>>(8*(keySize-i)))&0xFF ) 
		state = sbox(state) 								
		tmp = (state& 0xFF) << 8*(blockSize-1) 				
		state = (state>>8) | tmp               				
	return state





def invSbox(state):
	result = 0
	loops = blockSize*8//sboxSize
	for i in range(loops):
		result=result<<sboxSize
		result = result | invbox[(state>>sboxSize*(loops-i-1))&0x1F]
	return result

def decrypt_block(cipher, key, keySize):
	state = cipher
	state = addKey(state, (key&0xFF))
	for i in range(1,keySize):
		tmp = state>>(8*(blockSize-1)) 	
		state = ((state << 8) & 0xFFFFFFFFFF) | tmp 
		state = invSbox(state)
		state = addKey(state, (key>>(8*(i)))&0xFF)
	return state

#Worker thread for decryption
def decrypt_worker(low, high, numThread):
	i = low
	while (i<high and not event.is_set()):
		if i%100000 == 0:
			print(i)
			print("Thread ", numThread,":   " ,((i-low)/(high-low))*100,  "%")

		temp = decrypt_block(int(data[1][1],16), i, 4)
		global encrypt
		if temp in encrypt:
			if (verify_key(encrypt[temp],i)):
				begin = encrypt[temp]
				end = i
				key = (begin<<32)&0xFFFFFFFFFFFFFF | end&0xFFFFFFFF

				f = open( 'keySol.py', 'w' )
				f.write( "\n\n MITM HEX:" + myhex(temp) + "  key:   " +myhex(key) )
				f.close()
				event.set()

				return 1

		i += 1
		
	return 0

#Worker thread for encryption
def encrypt_worker(low,high, numThread):
	for i in range(low,high):
		if i%100000 == 0:
				print("Thread ", numThread, ":   " ,int(((i-low)/(high-low))*100),  "%")
		queue.put((encrypt_block_loop(int(data[1][0],16), i, 3),i))
		global maxEncrypt
		if i == (high-1):
			queue.put('STOP')

# Launches the encryption of 3 bytes and saves the encrypted messages in a dictionnary then
# launches the decryption of 4 bytes and check if it exists in the encryption dictionnary
def crack():

	with open('teamD.csv', 'Ur') as f:
		global data
		data = list(tuple(rec) for rec in csv.reader(f, delimiter=','))
	

	numthreads = 3
	pool = Pool(numthreads)


	for i in range(numthreads):
		pool.apply_async(encrypt_worker, args=(i*(maxEncrypt)//numthreads, (i+1)*(maxEncrypt+1)//numthreads, i))

	stopSIG = 0;
	j = 0
	while True:
		if not queue.empty():
			if j%20000 == 0:
				print ("List building:  ", 100*j//maxEncrypt, "  %")
			flag = queue.get()
			if flag == 'STOP' :
				stopSIG += 1
				if stopSIG == numthreads:
					break
			else:
				(halfcipher,key) = flag
				encrypt[halfcipher] = key 
				j += 1
	pool.close()
	pool.join()	
		
	print("\n----------[HALFCIPHER, KEY] TABLE FINISHED--------------\n")

	numthreads = 4

	pool2 = Pool(numthreads)


	min = int(sys.argv[1])*4294967295//11   # BUILD TO RUN ON 10 COMPUTERS
	max = (int(sys.argv[1])+1)*4294967295//11 
	

	for i in range(numthreads):
		pool2.apply_async(decrypt_worker, args=( i*(max-min)//numthreads+min, (i+1)*(max-min)//numthreads+min,i))
	pool2.close()
	pool2.join()	

# Check if the key found works for the rest of the csv file
def verify_key(begin, end):
	print("verify:", begin, end)
	key = (begin<<32)&0xFFFFFFFFFFFFFF | end&0xFFFFFFFF
	for msg,cipher in data:
		if msg != 'message hex':
			if encrypt_block(int(msg,16), key, 7) != int(cipher,16):
				return False 
	return True

def example():
	msg = 0x02F84B5E60
	#msg2 = 0x3CB04AC104
	#msg3 = 0xEC6A32DEFF
	#msg4 = 0x8598D4B3B1
	fullkey = 0x88FDC43854711C
	halfkey = 0x88FDC4 
	halfkey2 = 0x3854711C

	cipher = encrypt_block ( msg, fullkey, 7 )
	print("cipher (by Ek(p)):\t", myhex(msg) , myhex(cipher) )
	#cipher = encrypt_block ( msg2, fullkey, 7 )
	#print("cipher (by Ek(p)):\t", myhex(msg2) , myhex(cipher) )
	#cipher = encrypt_block ( msg3, fullkey, 7 )
	#print("cipher (by Ek(p)):\t", myhex(msg3) , myhex(cipher) )
	#cipher = encrypt_block ( msg4, fullkey, 7 )
	#print("cipher (by Ek(p)):\t", myhex(msg4) , myhex(cipher) )

	halfcipher = encrypt_block_loop(msg, halfkey, 3)
	print("halfcipher:\t", myhex(halfcipher))

	cipher = encrypt_block ( encrypt_block_loop(msg, halfkey, 3), halfkey2, 4 )
	print("cipher (by Ek1(Ek2(p)):\t", myhex(cipher) )



#example()
crack()