#!python3
# "bad block cipher"

import random
import csv
from bisect import bisect_left


sboxSize = 5
blockSize = 5
data = []
def myhex(state):
	return "0x%0.10X" % state

def addKey(state, key):
	#print("	addkey:")
	#print("		state:\t", myhex(state))
	#print("		key:\t", myhex(key))
	expansion = 0
	for i in range(blockSize):  
		expansion  = expansion<<8
		key0 = key
		if (i%2==1):
			key0 = key ^ 0xFF
		expansion = expansion | key0
	#	print("		expansion:\t", myhex(expansion))


	#print("		state^expansion:\t", myhex(state), myhex(expansion))	
	#print("		fin du addkey:",myhex(state ^ expansion))
	return state ^ expansion

def sbox(state):
	#print("	sbox:")
	box = [22, 0, 19, 9, 15, 3, 21, 18, 4, 26, 28, 13, 27, 5, 25, 31, 29, 12, 24, 6, 23, 8, 2, 11, 16, 30, 14, 10, 20, 7, 17, 1]
	result = 0
	loops = blockSize*8//sboxSize
	for i in range(loops):
		#print("		result:\t", myhex(result))
		result=result<<sboxSize
		#print("		result:\t", myhex(result))
		result = result | box[(state>>sboxSize*(loops-i-1))&0x1F]
		#print("		result:\t", myhex(result))
	
	return result

def encrypt_block(message, key, keySize):
	#if len("%0.10X"%message)//2!= blockSize or len("%0.14X" %key)//2!=keySize :
	#	return None # bad length
	state = message
	for i in range(1,keySize):
		# xor key byte
		#print("1	state:\t", myhex(state))
		state = addKey(state, (key>>(8*(keySize-i)))&0xFF ) # On crée à partir de 2 HEX de la clé un "expansion" de 10 HEX et on le XOR au state
		#print("2	state:\t", myhex(state))
		state = sbox(state) 								# Sboxsize bits par Sboxsize bits, on va remplacer les bits par ceux indiqués dans les Sbox par les Sboxsizes bits du state où on est (on commence par la gauche de state, sboxsize bits par sboxsize bits)
		#print("3	state:\t", myhex(state))
		tmp = (state& 0xFF) << 8*(blockSize-1) 				#  On met les
		#print("4	state:\t", myhex(state))   				#  2 derniers HEX	
		state = (state>>8) | tmp               				#  en premier
	state = addKey(state, key&0xFF ) 
	return state

def encrypt_block_loop(message, key, keySize):
	#if len("%0.10X"%message)//2!= blockSize or len("%0.14X" %key)//2!=keySize :
	#	return None # bad length
	state = message
	for i in range(1,keySize+1):
		# xor key byte
		#print("1	state:\t", myhex(state))
		state = addKey(state, (key>>(8*(keySize-i)))&0xFF ) # On crée à partir de 2 HEX de la clé un "expansion" de 10 HEX et on le XOR au state
		#print("2	state:\t", myhex(state))
		state = sbox(state) 								# Sboxsize bits par Sboxsize bits, on va remplacer les bits par ceux indiqués dans les Sbox par les Sboxsizes bits du state où on est (on commence par la gauche de state, sboxsize bits par sboxsize bits)
		#print("3	state:\t", myhex(state))
		tmp = (state& 0xFF) << 8*(blockSize-1) 				#  On met les
		#print("4	state:\t", myhex(state))   				#  2 derniers HEX	
		state = (state>>8) | tmp               				#  en premier
	return state





def invSbox(state):
	box = [22, 0, 19, 9, 15, 3, 21, 18, 4, 26, 28, 13, 27, 5, 25, 31, 29, 12, 24, 6, 23, 8, 2, 11, 16, 30, 14, 10, 20, 7, 17, 1]
	result = 0
	loops = blockSize*8//sboxSize
	for i in range(loops):





		#print("		result:\t", myhex(result))
		result=result<<sboxSize
		#print("		result:\t", myhex(result))
		result = result | box.index((state>>sboxSize*(loops-i-1))&0x1F)
		#print("		result:\t", myhex(result))
	
	return result

def decrypt_block(cipher, key, keySize):
	#if len("%0.10X"%cipher)//2!= blockSize or len("%0.14X" %key)//2!=keySize :
	#	return None # bad length
	state = cipher

	#On fait addkey
	#state = addKey(state, (key>>(8*(keySize-1)))&0xFF)
	state = addKey(state, (key&0xFF))
	
	for i in range(1,keySize):
		
		#On met les 2 premiers en derniers:
		tmp = state>>(8*(blockSize-1)) 	
		state = ((state << 8) & 0xFFFFFFFFFF) | tmp 
		
		#On effectue l'inverse des Sbox:
		state = invSbox(state)

		#On fait addkey
		state = addKey(state, (key>>(8*(i)))&0xFF)

	return state

def crack():

	with open('teamC.csv', 'Ur') as f:
		global data
		data = list(tuple(rec) for rec in csv.reader(f, delimiter=','))
	encrypt = {}
	#for i in range(12648420,12648440):
	for i in range(0,16777215):
		encrypt[myhex(encrypt_block_loop(int(data[1][0],16), i, 3))] = myhex(i) 
		#encrypt[myhex(encrypt_block_loop(0x00DEADBEEF, i, 3))] = i
		if i%100000 == 0:
			print(i , "  on   " , 16777215)
	print("\n\n")

	#for i in range(364969956,364969976):
	for i in range(0,4294967295):

		if i%100000 == 0:
			print(i , "   on   "  , 4294967295)

		temp = myhex(decrypt_block(int(data[1][1],16), i, 4))
		#temp = myhex(decrypt_block(0x1215531E45, i, 4))
		if temp in encrypt:
			if (verify_key(i, encrypt[temp])):
				f = open( 'keySol.py', 'w' )
				f.write( "\n\n" + temp + "  key:   " +myhex(encrypt[temp]) + myhex(i) )
				f.close()
				return


def verify_key(begin, end):
	key = (begin<<32)&0xFFFFFF | end&0xFFFFFFFF
	print(data)
	
	iterdata = iter(data)
	next(iterdata)
	for msg,cipher in iterdata:
		if myhex(encrypt_block(int(msg,16), key, 7)) != cipher:
			return False 
	return True

def example():
	# pair of keys: 0x3F0011EA3F0010       0xFFFFFFFFFFFFFF 
	msg = 0x00DEADBEEF
	#msg = 0x3CB04AC104
	fullkey = 0xC0FFEE15C0FFEE
	#fullkey = 0x3F0011EFFFFFFF 
	halfkey = 0xC0FFEE 
	halfkey2 = 0x15C0FFEE 

	cipher = encrypt_block ( msg, fullkey, 7 )
	print("cipher (by Ek(p)):\t", myhex(cipher) )

	halfcipher = encrypt_block_loop(msg, halfkey, 3)
	print("halfcipher:\t", myhex(halfcipher))

	cipher = encrypt_block ( encrypt_block_loop(msg, halfkey, 3), halfkey2, 4 )
	print("cipher (by Ek1(Ek2(p)):\t", myhex(cipher) )

	



	print("verification:")
	print("ciphering the mesg with halfkey:",myhex(encrypt_block_loop(msg, halfkey, 3)))
	print("deciphering the cipher with halfkey2:",myhex(decrypt_block(cipher, halfkey2, 4)))





#example()
crack()